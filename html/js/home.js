var $form = $("form");

$.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z," "]+$/i.test(value);
}, "Por favor solo letras");

$form.validate({
    rules: {
        nombres: {
            required: true,
            minlength: 3,
            lettersonly: true
        },
        email: {
            required: true,
            email: true
        },
        celular:{
            required: true,
            digits: true,
            minlength: 7,
            maxlength: 9
        },
        consulta:{
            required: true
        }
    },
    messages: {
        nombres:{
            required: 'Este campo es requerido',
            minlength: 'El texto es muy corto',
            lettersonly: 'Por favor, solo ingresa letras'
        },
        email: {
            required: 'Este campo es requerido',
            email: 'Correo no válido'
        },
        celular:{
            required: 'Este campo es requerido',
            minlength: 'El texto es muy corto',
            maxlength: 'El texto es muy largo',
            digits: 'Por favor, solo ingresa números'
        },
        consulta:{
            required: 'Elija cual es su consulta'
        }
    },
    submitHandler: function() {
        console.log("correcto");
        alert('Solicitud enviado');
        // window.location.href = "http://www.w3schools.com";
    }
});