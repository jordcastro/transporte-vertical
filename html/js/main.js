// Detecciones de ie 9 e ie8 (si no se usaran eliminar)

//conditionizr.add('ie9', function () {
//    return (Function('/*@cc_on return (/^9/.test(@_jscript_version) && /MSIE 9\.0(?!.*IEMobile)/i.test(navigator.userAgent)); @*/')());
//});

// Que hacer cuando sean positivas las detecciones (eliminar si no se usaran)

/*conditionizr.config({
    assets: 'js/',
    tests: {
        'ie8': ['class']
    }
});*/

//conditionizr.polyfill('js/min/placeholders.min.js', ['ie9','ie8']); (ejemplo de polyfill)


const chatwhatsapp = '<a class="icono btn-whatsapp" href="#"><i class="fab fa-whatsapp"></i></a>'

const ChatWidget = document.getElementById("chatwidget")

function WidgetChatbox (){
    if (ChatWidget) {
        return ChatWidget.innerHTML = chatwhatsapp
    }
}


WidgetChatbox()




// function WidgetChatbox2 (link,classe,contenido){
//   const chatwhatsapp2 = '<a href="'+ link +'" class="'+ classe +'">'+ contenido +'</a>'
//     if (ChatWidget) {
//         return ChatWidget.createElement(chatwhatsapp2)
//     }
// }


// WidgetChatbox2('#','btn-whatsapp2', 'puto')





// HEADER HTML
const homepage = "index.html"
const aboutpage = "nosotros.html"
const ingepage = "ingenieria.html"
const rentpage = "alquiler.html"
const mantenimiento = "mantenimiento.html"

const headerContenido = '<div class="header_sup">' +
      '<ul class="container menu-contact-top">' +
        '<li><a  class="icono" href="mailto:ingenieria@transportevertical.com.pe"><i class="far fa-envelope"></i>ingenieria@transportevertical.com.pe</a></li>' +
        '<li><a  class="icono" href="tel:992189749"><i class="fas fa-mobile-alt"></i>992189749 </a></li> ' +
        '<li><a  class="icono" href="tel:941387440"><i class="fas fa-mobile-alt"></i>941387440 </a></li> ' +
      '</ul> ' +
    '</div> ' +
    '<div class="header_inf"> ' +
      '<div class="container"> ' +
          '<a href="'+ homepage + '" class="logo-header"> ' +
            '<img class="logo-nav"src="images/logo-principal.svg" alt="Transporte Vertical"> ' +
          '</a> ' +
          '<div class="btn-burguernav"><button class=""><i></i></button></div> ' +
          '<nav class="nav_box-head"> ' +
            '<ul class="menu-principal"> ' +
               '<li><a href="'+ aboutpage +'">EMPRESA</a></li> ' +
               '<li><a href="'+ ingepage +'">INGENIERÍA</a>'+
                '<ul>' +
                  '<li><a href="'+ ingepage +'">Maniobras</a></li>' +
                  '<li><a href="'+ ingepage +'">Montajes y desmontaje de equipos</a></li>' +
                  '<li><a href="'+ ingepage +'">Riggin Plan</a></li>' +
                '</ul>' +
               '</li>' +
               '<li><a href="'+ rentpage +'">ALQUILERES</a>'+
                '<ul>' +
                  '<li><a href="'+ rentpage +'">Minicargador</a></li>' +
                  '<li><a href="'+ rentpage +'">Telehandler</a></li>' +
                  '<li><a href="'+ rentpage +'">Rodillo</a></li>' +
                  '<li><a href="'+ rentpage +'">Manlift</a></li>' +
                  '<li><a href="'+ rentpage +'">Camion Grúa</a></li>' +
                  '<li><a href="'+ rentpage +'">Retroexcavadora</a></li>' +
                '</ul>' +
               '</li>' +
               '<li><a href="'+ mantenimiento +'">MANTENIMIENTO</a></li>' +
               '<li><a href="contact.html">CONTÁCTANOS</a></li>' +
            '</ul>' +
            '<div class="navList_socialmedia">' +
                '<span>Siguenos en:</span>' +
                '<ul class="menu-socialmedia">' +
                 '<li class=""><a href="#"><i class="fab fa-facebook-f"></i></a></li>' +
                  '<li class=""><a href="#"><i class="fab fa-tumblr"></i></a></li>' +
                  '<li class=""><a href="#"><i class="fab fa-whatsapp"></i></a></li>' +
                  '<li class=""><a href="#"><i class="fab fa-linkedin-in"></i></a></li>' +
                '</ul>' +
            '</div>' +
          '</nav>' +
        '</div>' +
    '</div>';
const header = document.getElementById("header")




function navcontenttop(){
    if (header) {
        return header.innerHTML = headerContenido

    }

}
navcontenttop()

//  MENU
var openmenu = $('.btn-burguernav');
var mainmenu = $('.nav_box-head');
openmenu.click(function(event) {
    /* Act on the event */
    mainmenu.slideToggle('slow');
});

const footerContenido = '<div class="container content_footer">' +
        '<div class="col_item_footer">' +
           '<a href="'+ homepage + '""><img class="logo-footer" src="images/logo-principal-white.svg" alt=""></a>' +
           '<div class="text">' +
             '<p>Garantizamos todas nuestras maquinarios con un servicio bueno y comodo para el final de su obra</p>' +
             '<a class="icono" href="#"><i class="fas fa-map-marker-alt"></i>Calle por informar, Distrito falta, Lima N° 6789</a>' +
           '</div>' +
        '</div>' +
        '<div class="col_item_footer">' +
          '<h3>Enlaces:</h3>' +
          '<ul class="menu-navfooter">' +
            '<li><a href="nosotros.html">¿Quiénes somos?</a></li>' +
            '<li><a href="ingenieria.html">Ingeniería</a></li>' +
            '<li><a href="alquiler.html">Alquileres</a></li>' +
            '<li><a href="mantenimiento.html">Mantenimiento</a></li>' +
            '<li><a href="contact.html">Contáctanos</a></li>' +
          '</ul>' +
        '</div>' +

        '<div class="col_item_footer">' +
          '<h3>Emails:</h3>' +
          '<ul class="menu-contact-bottom">' +
            '<li><a class="icono" href="mailto:ingenieria@transportevertical.com.pe">' +
                '<i class="far fa-envelope"></i>ingenieria@transportevertical.com.pe</a></li>' +
            '<li><a class="icono" href="mailto:operaciones@transportevertical.com.pe">' +
                '<i class="far fa-envelope"></i>operaciones@transportevertical.com.pe</a></li>' +
          '</ul>' +
          '<h3>Telefonos:</h3>' +
          '<ul class="menu-contact-bottom">' +
            '<li><a class="icono" href="tel:992189749"><i class="fas fa-mobile-alt"></i>992189749</a></li>' +
            '<li><a class="icono" href="tel:941387440"><i class="fas fa-mobile-alt"></i>941387440</a></li>' +
            '<li><a class="icono" href="tel:014612545"><i class="fas fa-phone-alt"></i>(01) 461-2545</a></li>' +
          '</ul>' +
        '</div>' +

        '<div class="col_item_footer">' +
          '<h3>Síguenos en:</h3>' +
          '<ul class="menu_socialfooter">' +
            '<li><a class="icono_only" href=""><i class="fab fa-facebook-f"></i></a></li>' +
            '<li><a class="icono_only" href=""><i class="fab fa-tumblr"></i></a></li>' +
            '<li><a class="icono_only" href=""><i class="fab fa-whatsapp"></i></a></li>' +
            '<li><a class="icono_only" href=""><i class="fab fa-linkedin-in"></i></a></li>' +
          '</ul>' +
        '</div>' +
      '</div>';


const footer = document.getElementById("footer")
// const b = document.getElementById(id);
function navcontentbottom(){
  if (footer) {
      return footer.innerHTML = footerContenido
  }
  
}
navcontentbottom()
// Main
var ancho = $(window).width();
var alto = $(window).height();

var MyApp;

var MyApp = {

    home: {
        banner: function(){
            // calcular alto banner home
            //76px (header) + 100px(cajas) = 176
            var alto_imagen = alto;
            var alto_banner = alto_imagen;
            //alert(alto_banner);
            $('.cabecera-banner-home').css('height', alto_banner+'px');

            $(window).resize(function() {
                //76px (header) + 100px(cajas) = 176
                var alto_imagen = $(window).height();
                var alto_banner = alto_imagen;
                //alert(ancho_banner);
                $('.banner-home').css('height', alto_banner+'px');
            });
        },

        banner_home: function() {
            $(function() {
              // $('#slides').superslides({
              //   hashchange: true
              // });
              $('#slides').superslides({
                inherit_width_from: '.cabecera-banner-home',
                inherit_height_from: '.cabecera-banner-home'
              });

            });
        },

        servicios_home: function(){
            $('.owl-servicios').owlCarousel({
                loop: false,
                margin:0,
                nav:true,
                slideBy: 1,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3,
                        slideBy: 3
                    }
                }
            });
        },
    }
}

$(document).ready(function(){
    if ($('.cabecera-banner-home').length) {
        MyApp.home.banner();
    };
    if ($('.cabecera-banner-home').length) {
        MyApp.home.banner_home();
    };
    if ($('.servicios-home').length) {
        MyApp.home.servicios_home();
    };

});

// const banerInternas = $(".baner-interna");
// const HeaderHeight = $("#header").height();

// if (banerInternas) {
//     banerInternas.css('margin-top', HeaderHeight +'px');
// }

var position = $(window).scrollTop(); 

// should start at 0

$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll !== 0) {
      $(header).addClass('bg-white');
    }else{
      $(header).removeClass('bg-white');
    }



    if(scroll > position) {
        // console.log('scrollDown');
        $(header).addClass('js_header_up');
        $('header > .header_sup').slideUp('fast');
    } else {
         // console.log('scrollUp');
        $(header).removeClass('js_header_up');
        $('header > .header_sup').slideDown('fast');
    }
    position = scroll;
});




$('tbody tr').mouseover(function(){

  $(this).addClass('current');

});



$('tbody tr').mouseout(function(){

  $(this).removeClass('current');

});
//create var for points
// var points = '0';

//This adds the hover function to change to blue on mouseover then back to green on mouseout
// let  box = $(' .menu-principal > li ');
// let  box2 = $(' .menu-principal > li > ul ');

// box.hover( function() {

//     $($(this) > box2).slideDown('slow');
//     // box2.slideToggle('slow');
//     // alert('aqui hay un bombon');
//     // $( this ).css( "background","blue" );
//     // points++; //add +1 to points
    
//     //redisplay the points with the new value by replacing the value of the whole div
//     // $('div#pointdisplay').html('Points: '+ points); 

//     }, function() {
//       $($(this) > box2).slideUp('slow');
//       // box2.slideToggle('slow');
//     // $( this ).css( "background","green" );
//   }
// );
// box.hover(function() {
//   /* Stuff to do when the mouse enters the element */

// }, function() {
//   /* Stuff to do when the mouse leaves the element */

// });